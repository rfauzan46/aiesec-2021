from django.urls import path, include
from .views import abroad

urlpatterns = [
    path('', abroad, name='abroad'),
]
