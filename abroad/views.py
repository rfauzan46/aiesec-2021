from django.shortcuts import render

# Create your views here.
def abroad(request):
    return render(request, 'abroad.html')